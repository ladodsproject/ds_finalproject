package edu.nccu.misds.dsproject;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class BeatGoogle {
	private ArrayList<Keyword> keywords;
	
	public BeatGoogle(ArrayList<Keyword> k){
		this.keywords = k;
	}
	
	public void search(String searchKeyword,PrintWriter webWriter) throws IOException{
		GoogleQuery google = new GoogleQuery(searchKeyword);
		HashMap<String, String> queryResults = google.query();
		ArrayList<Website> webRoots = new ArrayList<Website>();
		
		for(Entry<String,String> queryResult : queryResults.entrySet()){
			String url = queryResult.getValue().toLowerCase();
			if(url.isEmpty()){
				continue;
			}
			
			if(!url.startsWith("http")){
				url = "http://" + url;
			}
			String name = queryResult.getKey();
			webRoots.add(new Website(url,name));
		}
		for(Website webRoot : webRoots){
			webRoot.evaluate(keywords);
			calGlobalScore(webRoot);
		}
		ArrayList<Website> sortedWebRoots = QuickSort(webRoots);
		for(Website website :sortedWebRoots){
			website.print(webWriter);
		}
	}
	public void calGlobalScore(Website node){
		for(Website child :node.getChildren()){
			calGlobalScore(child);
		}
		node.globalScore = node.localScore;
		for(Website child : node.getChildren()){
			node.globalScore+=child.globalScore;
		}
	}
	public ArrayList<Website> QuickSort(ArrayList<Website> list) {
		QuickSort(list , 0 , list.size()-1);
		return list;
	}
	
	private void QuickSort(ArrayList<Website> list, int left, int right){
		if(left>right){
			return;
		}
		
		int pivotIndex = (left+right)/2;
		Website pivot = list.get(pivotIndex);
		swap(list, pivotIndex, right);
		
		int swapIndex = left;
	    for (int i = left; i < right; i++){
	    	if (list.get(i).globalScore >= pivot.globalScore){
	    		swap(list, i, swapIndex);
	    		swapIndex++;
	    	}
	    }
	    swap(list, swapIndex, right);
	    
	    QuickSort(list, left, swapIndex - 1);
	    QuickSort(list, swapIndex + 1, right);
    }
	
	public void swap(ArrayList<Website> arr,int left,int right){
		Website temp = arr.get(left);
		arr.add(left,arr.get(right));
		arr.remove(left+1);
		arr.add(right,temp);
		arr.remove(right+1);
	}
}
