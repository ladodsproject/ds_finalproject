package edu.nccu.misds.dsproject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Website {
	private Website parent;
	private ArrayList<Website> children;
	private String content;
	
	public String url;
	public String name;
	public int globalScore;
	public int localScore;
	
	public Website(String url,String name){
		this.url = url;
		this.name = name;
		children = new ArrayList<Website>();
	}
	public Website(String url){
		this(url,"Unknown");
	}
	
	@Override
	public String toString() {
		return "["+url+","+name+","+localScore+","+globalScore+"]";
	}
	
	public Website getParent(){
		return parent;
	}
	
	public ArrayList<Website> getChildren(){
		return children;
	}
	
	public int getDepth(){
		int retVal = 0;
		Website currNode = this;
		while(currNode.parent!=null)
		{
			retVal ++;
			currNode = currNode.parent;
		}
		return retVal;
	}
	
	public String fetchContent() throws IOException{
		
		URL u = new URL(url);
		URLConnection connection = u.openConnection();
		connection.setRequestProperty("User-agent","Chrome/7.0.517.44");
		BufferedReader buffer = new BufferedReader(new InputStreamReader(connection.getInputStream(),"utf-8"));
		
		String reValue = null;
		String line;
		while((line=buffer.readLine())!=null){  // line=buffer.readLine(); line!=null
			reValue += line; // reValue = reValue + line
		}	
		return reValue;
	}
	
	public void evaluate(List<Keyword> keywords) throws IOException{
		System.out.println("Evaluating: "+url);
		try{
			if(content==null){
				content = fetchContent();
			}
			Document doc = Jsoup.parse(content);
			Elements titles = doc.select("title");
			if(titles.size()>0){
				name = titles.get(0).text();
				System.out.println(name);
			}
			for(Keyword k : keywords){
				int times = count(k.name);
				localScore += times*k.weight;
				k.count += times;
			}
			if(getDepth()<1){
				extendNextLayer(doc,keywords);
			}
		}
		catch(IOException e){
			name = "PageNotFound";
			localScore = 0;
		}
	}
	public int count(String keyword) throws IOException{
		if(content == null){
			content = fetchContent();
		}
		
		content = content.toUpperCase();
		keyword = keyword.toUpperCase();
		
		int reValue = 0;
		int from = 0;
		while((from=content.indexOf(keyword,from))!=-1){
			reValue++;
			from += keyword.length();
		}
		return reValue;
	}
	
	public void extendNextLayer(Document doc,List<Keyword> keywords) throws IOException{
		Elements links = doc.select("a[href]");
		for(Element link : links){
			if(getChildren().size()>2){
				break;
			}
			
			String href = link.attr("href").toLowerCase().trim();
			
			if(href.startsWith("javascript:")){
				continue;
			}
			Website child = new Website(href);
			this.children.add(child);
			child.parent = this;
		}
		
		for(Website ChildWeb : getChildren()){
			ChildWeb.evaluate(keywords);
		}
	}
	
	public void print(PrintWriter webWriter){
		StringBuilder sb = new StringBuilder();
		for(int i =0;i<getDepth();i++){
			sb.append("\t");
		}
		sb.append(this.toString());
		
		webWriter.println("<a href="+url+">"+name+"</a>"+" 該網頁得分是: "+globalScore);
		webWriter.println("</br>");
		
		System.out.println(sb);
	}
}