package edu.nccu.misds.dsproject;

public class Keyword {
	String name;
	int count;
	double weight;
	
	public Keyword(String n,double w){
		this.name=n;
		this.weight=w;
	}
	
	public String toString(){
		return "["+name+","+count+","+weight+"]";
	}
}
