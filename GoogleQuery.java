package edu.nccu.misds.dsproject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GoogleQuery {
	
	public String searchkeyword; 
	public String url;
	public String content;
	
	public GoogleQuery(String searchkey){
		this.searchkeyword=searchkey;
		this.url = "http://www.google.com/search?q="+searchkeyword+"&oe=utf8&num=10&lr=lang_zh-TW&cr=countryTW&as_eq=PIXNET+巴哈姆特+博客來";
	}
	
	public String fetchContent() throws IOException{
		
		URL u = new URL(url);
		URLConnection connection = u.openConnection();
		connection.setRequestProperty("User-agent","Chrome/7.0.517.44");
		BufferedReader buffer = new BufferedReader(new InputStreamReader(connection.getInputStream(),"utf-8"));
		
		String reValue = null;
		String line;
		while((line=buffer.readLine())!=null){  // line=buffer.readLine(); line!=null
			reValue += line; // reValue = reValue + line
		}	
		return reValue;
	}
	
	public HashMap<String, String> query() throws IOException{
		if(content==null){
			content = fetchContent();
		}

		HashMap<String, String> retVal = new HashMap<String, String>(); 

		Document doc = Jsoup.parse(content);
		Elements liList = doc.select("li.g");
		
		for(Element li : liList){
			try{	
				Element h3 = li.select("h3.r").get(0);
				String title = h3.text();
				
				Element cite = li.select("cite").get(0);
				String citeUrl = cite.text();
				System.out.println(title+" "+citeUrl);
				retVal.put(title, citeUrl);
			}
			catch(IndexOutOfBoundsException a){
			}
		}
		return retVal;
	}
}
