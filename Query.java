package edu.nccu.misds.dsproject;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Query
 */
@WebServlet("/Query")
public class Query extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Query() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");				
		String searchKeyword = request.getParameter("fieldA");
		PrintWriter webWriter = new PrintWriter(response.getOutputStream());
		if(searchKeyword==null){					
			webWriter.println("<html>");
			 webWriter.println("<head> ");
			  webWriter.println("<title> BeatGoogle!!!中文編碼成功 </title>");
			 webWriter.println("</head>");
			webWriter.println("<body background=\"http://7-themes.com/data_images/out/70/7010824-leaves-white-background.jpg\">");
			  webWriter.println("</br></br></br></br></br></br></br></br>");
			 webWriter.println("<center>  <b> <font face=\"cursive\" color=\"\" size=\"7\">Be a one day hipster ~ </font></b>");
			  webWriter.println("</br></br>");
			  webWriter.println("<form action=\"http://dsfinalmyladoproject.eu-gb.mybluemix.net/Query\">");
			  webWriter.println("<input type=\"text\" style=\" text-align:center; font-size: 20px;\" size=\"60\" name=\"fieldA\" value=\"\" onkeypress=\"enter()\"></input>");
			  webWriter.println("<button type=\"submit\" style=\"width:80px;height:29px;font-size:20px;\"> Go!! </button>");
			  webWriter.println("</form>");
			  webWriter.println("</br>");
			  webWriter.println("<marquee width=\"700\" behavior=\"scroll\">BeatGoogle!!!!! Be a one day hipster ~~~        請輸入你想參觀的展覽~~~ </marquee>");
			 webWriter.println("</center>");
			webWriter.println("</body>");
			webWriter.println("</html>");
		}else{
		
		
			ArrayList<Keyword> keywords = new ArrayList<Keyword>();
			keywords.add(new Keyword("www.npm.gov", 20000.0));
			keywords.add(new Keyword("cksmh.gov.tw", 1000.0));
			keywords.add(new Keyword("twtc.com.tw", 5000.0));
			keywords.add(new Keyword("twtcnangang.com.tw", 5000.0));			
			keywords.add(new Keyword("http://www.huashan1914.com/", 40000.0));	
			keywords.add(new Keyword("tfam.museum", 2000.0));
			keywords.add(new Keyword("Taipei Fine Arts Museum © 2010-2014 TFAM", 10.0));
			keywords.add(new Keyword("npac-ntch.org/", 50000.0));			
			keywords.add(new Keyword("www.songshanculturalpark.org", 50000.0));
			keywords.add(new Keyword("taipeiarena.com", 1000.0));
			keywords.add(new Keyword("redhouse.org.tw", 1000.0));
			keywords.add(new Keyword(".tmseh.taipei.gov.tw", 500.0));
			keywords.add(new Keyword("ntm.gov.tw", 5000.0));
			keywords.add(new Keyword("taipei-expopark.tw", 1000.0));
			keywords.add(new Keyword("ntsec.gov.tw", 1000.0));
			keywords.add(new Keyword("citytalk.tw", 1000.0));
			keywords.add(new Keyword("Citytalk", 500.0));
			keywords.add(new Keyword("展覽", 20.0));
			keywords.add(new Keyword("Exhibition", 5.0));
			keywords.add(new Keyword("表演", 5.0));
			keywords.add(new Keyword("迪士尼90周年特展", 200.0));
			keywords.add(new Keyword("華山1914文化創意產業園區", 50.0));
			keywords.add(new Keyword("國立故宮博物院", 50.0));
			keywords.add(new Keyword("disney90.com.tw/", 30000.0));
			keywords.add(new Keyword("書展", 5.0));
			keywords.add(new Keyword("Book exhibition", 5.0));
			keywords.add(new Keyword("展覽資訊", 20.0));
			keywords.add(new Keyword("Art exhibition", 5.0));
			keywords.add(new Keyword("戲劇表演", 5.0));
			keywords.add(new Keyword("小王子特展", 300.0));
			keywords.add(new Keyword("演唱會", 5.0));
			keywords.add(new Keyword("音樂會", 5.0));
			keywords.add(new Keyword("Concert", 5.0));
			keywords.add(new Keyword("音樂劇", 5.0));
			keywords.add(new Keyword("Musical", 5.0));
			keywords.add(new Keyword("歌劇", 5.0));
			keywords.add(new Keyword("藝術展", 20.0));
			keywords.add(new Keyword("臺北", 2.0));
			keywords.add(new Keyword("台北", 2.0));
			keywords.add(new Keyword("Taipei", 2.0));
			keywords.add(new Keyword("ibon", -1000.0));
			keywords.add(new Keyword("高雄市立", -100000.0));
//			keywords.add(new Keyword("plurk", -1000.0));
//			keywords.add(new Keyword("噗浪", -1000.0));
//			keywords.add(new Keyword("facebook", -1000.0));
//			keywords.add(new Keyword("臉書", -1000.0));
//			keywords.add(new Keyword("yam", -1000.0));
//			keywords.add(new Keyword("蕃薯藤", -1000.0));
//			keywords.add(new Keyword("xuite", -1000.0));
//			keywords.add(new Keyword("隨意窩", -1000.0));
//			keywords.add(new Keyword("博客來", -1000.0));
			
			webWriter.println("<html>");
			webWriter.println("<head> ");
			webWriter.println("<title> BeatGoogle!!!中文編碼成功 </title>");
			webWriter.println("</head>");
			webWriter.println("<body background=\"http://7-themes.com/data_images/out/70/7010824-leaves-white-background.jpg\">");
			
			webWriter.println("您所搜尋的是 ... "+searchKeyword);
			webWriter.println("</br>");
			webWriter.println("</br>");
			webWriter.println("為您找到的資訊 ... ");
			webWriter.println("</br>");
			webWriter.println("</br>");
			BeatGoogle beatGoogle = new BeatGoogle(keywords);
			beatGoogle.search(searchKeyword,webWriter);			
			webWriter.println("</body>");
			webWriter.println("</html>");		
		}
		webWriter.flush();
		webWriter.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
